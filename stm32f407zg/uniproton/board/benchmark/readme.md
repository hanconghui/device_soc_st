# 源码下载
获取OpenHarmony源码：[下载说明](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/sourcecode-acquire.md)
# 编译
## 1、hb set
```python
选择 "rtos_kernel_benchmark"
```
## 2、hb build -f
## 3、烧录
```python
1.Windows10安装CH341串口驱动.
2.安装烧录工具ST-LINK Utility.
3.连接开发板烧录串口,进入烧录工具ST-LINK Utility,选取下拉菜单Target中的Program&Verify(或直接按下快捷键ctrl+p).
4.XTS测试:选取out/st/路径下的rtos_kernel_benchmark目录的OHOS_Image.bin文件并点击Start开始烧录.
5.通过串口工具XCOM来查看开发板通过串口打印的输出,波特率选择115200.
```
# 测试结果查看
```python
############## Uniproton test ###############
perfTest setup
##################start DEADLOCKBREAKTEST##################
###############end DEADLOCKBREAKTEST cycle=1030###############
../../../../test/xts/acts/kernel_lite/uniproton_perf_testing/src/perfTest.c:90:DEADLOCKBREAKTEST:PASS
##################start TASKPREEMPT##################
###############end TASKPREEMPT cycle=397###############
../../../../test/xts/acts/kernel_lite/uniproton_perf_testing/src/perfTest.c:99:TASKPREEMPT:PASS
##################start MESSAGELATENCY##################
###############end MESSAGELATENCY cycle=361###############
../../../../test/xts/acts/kernel_lite/uniproton_perf_testing/src/perfTest.c:108:MESSAGELATENCY:PASS
##################start INTERRUPTLATENCY##################
###############end INTERRUPTLATENCY cycle=150###############
../../../../test/xts/acts/kernel_lite/uniproton_perf_testing/src/perfTest.c:117:INTERRUPTLATENCY:PASS
##################start TASKSWITCH##################
###############end TASKSWITCH cycle=274###############
../../../../test/xts/acts/kernel_lite/uniproton_perf_testing/src/perfTest.c:126:TASKSWITCH:PASS
##################start SEMAPHORESHUFFLE##################
###############end SEMAPHORESHUFFLE cycle=801###############
../../../../test/xts/acts/kernel_lite/uniproton_perf_testing/src/perfTest.c:135:SEMAPHORESHUFFLE:PASS
perfTest teardown

------------------
6 Tests 0 Failures 0 Ignored
OK
ALL the test suites finished
```