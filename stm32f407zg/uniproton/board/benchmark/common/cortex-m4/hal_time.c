/*
 * Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
#include "rhealstone_test.h"
#include "misc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_rcc.h"
#include "prt_config.h"

#define TEST_CYCLE_TO_US_INT(cycle) (unsigned int)(((unsigned long long)(cycle) * 1000000) / OS_SYS_CLOCK)
#define TEST_CYCLE_TO_US_DEC(cycle) (unsigned int)(((((unsigned long long) \
(cycle) * 1000000) % OS_SYS_CLOCK) * 1000) / OS_SYS_CLOCK)
#define TEST_CYCLE_TO_US(cycle)     TEST_CYCLE_TO_US_INT(cycle)

int TestTimeInit(void)
{
    TIM_TimeBaseInitTypeDef timerInit = { 0 };

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseStructInit(&timerInit);
    timerInit.TIM_Period = 0xFFFFFFFF;
    timerInit.TIM_Prescaler = 0;
    TIM_TimeBaseInit(TIM2, &timerInit);

    TIM_ClearFlag(TIM2, TIM_FLAG_Update);
    TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);

    TIM_Cmd(TIM2, ENABLE);
    return 0;
}

unsigned int TestTimeGet(void)
{
    return TIM_GetCounter(TIM2);
}

unsigned int TestTimeUsGet(void)
{
    return TEST_CYCLE_TO_US(TIM_GetCounter(TIM2));
}

void RhealstoneTestTimeShow(const char *info, uint32_t count[TEST_RESULT_NUM], uint64_t sum[TEST_RESULT_NUM])
{
    uint32_t average[TEST_RESULT_NUM];

    average[TEST_RESULT] = sum[TEST_RESULT] / (count[TEST_COUNT_BASE] - count[TEST_COUNT_FAILED]);
    average[TEST_RESULT_HEAD] = sum[TEST_RESULT_HEAD] / (count[TEST_COUNT_BASE] - count[TEST_COUNT_FAILED]);

    printf("\r %-40s average = %d", info, average[TEST_RESULT_HEAD]);
}

void TestIntLock(void)
{
    __asm__ volatile("CPSID I");
}

void TestIntUnlock(void)
{
    __asm__ volatile("CPSIE I");
}
