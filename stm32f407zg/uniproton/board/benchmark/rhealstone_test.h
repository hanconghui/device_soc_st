/*
 * Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RHEALSTONE_TEST_H
#define RHEALSTONE_TEST_H

#include "stdio.h"

#include "stdint.h"

#include "stdbool.h"

#undef STATIC_INLINE
#define STATIC_INLINE static __inline

extern bool g_testDebugInfo;
#define PRINT_INFO(fmt, ...) do {               \
    if (g_testDebugInfo) {                      \
        printf("[INFO] " fmt, ##__VA_ARGS__);   \
    }                                           \
}while (0)

void RTOSJitterTest(void);
void RhealstoneTest(void);
void RTOSPerformanceTest(void);

int TestTimeInit(void);

unsigned int TestTimeGet(void);

unsigned int TestTimeUsGet(void);

#define TEST_RESULT_NUM    2
#define TEST_RESULT        0
#define TEST_RESULT_HEAD   1
#define TEST_COUNT_BASE    0
#define TEST_COUNT_FAILED  1

void RhealstoneTestTimeShow(const char *info, uint32_t count[TEST_RESULT_NUM], uint64_t sum[TEST_RESULT_NUM]);

void RhealstonePutTime(uint32_t count, uint32_t totalTime, uint32_t loopTime, \
                       uint32_t overheadTime, uint32_t result[TEST_RESULT_NUM]);

void TestIntLock(void);

void TestIntUnlock(void);

#define TEST_RESULT_CHECK_GOTO(val, info, lable) do {            \
    if ((val) < 0) {                                               \
        printf("[%s:%d] %s \n\r", __FUNCTION__, __LINE__, info); \
        goto lable;                                              \
    }                                                            \
} while (0)

#define TEST_RESULT_ASSERT(realVal, expect, lable) do {                                          \
    if ((realVal) != (expect)) {                                                                     \
        printf("[%s:%d] realVal: %u expect: %u\n", __FUNCTION__, __LINE__, (realVal), (expect)); \
        goto lable;                                                                              \
    }                                                                                            \
} while (0)

typedef void (*TEST_TASK_FUNC)(uintptr_t arg);

typedef struct {
    int highPrio;
    int midPrio;
    int lowPrio;
    int stackSize;
    int testCount;
} TaskPreempt;

int TestBaseTime(TaskPreempt *taskInfo);

int TaskPreemptTest(TaskPreempt *taskInfo);

int TaskSwitchTest(TaskPreempt *taskInfo);

typedef void (*TEST_HWI_FUNC)(void);

int RhealstoneBaseTime(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);

int RhealstonePreemptTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);

int RhealstoneSwitchTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);

int RhealstoneMessageLatencyTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);

int RhealstoneSemaphoreShuffleTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);

int RhealstoneInterruptLatencyTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);

int RhealstoneDeadlockBreakTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM]);
#endif