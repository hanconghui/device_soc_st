/*
 * Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HAL_UNIPROTON_H
#define HAL_UNIPROTON_H
#include "rhealstone_test.h"
#include "prt_task.h"
#include "prt_mem.h"
#include "prt_module.h"
#include "prt_hwi.h"
#include "prt_queue.h"
#include "prt_sem.h"
#include "misc.h"

#define __UNIPROTON__ 1

STATIC_INLINE int TestTaskCreate(const char *name, int priority, \
                                 unsigned int stackSize, TEST_TASK_FUNC func, uintptr_t *taskId)
{
    U32 ret;
    struct TskInitParam taskParam = {0};
    taskParam.taskEntry = (TskEntryFunc)func;
    taskParam.stackSize = stackSize;
    taskParam.name = name;
    taskParam.taskPrio = priority;
    taskParam.stackAddr = 0;

    ret = PRT_TaskCreate((TskHandle *)taskId, &taskParam);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestTaskSuspend(uintptr_t tasKId)
{
    U32 ret = PRT_TaskSuspend((TskHandle)tasKId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestTaskResume(uintptr_t tasKId)
{
    U32 ret = PRT_TaskResume((TskHandle)tasKId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestTaskYield(void)
{
    U32 ret = PRT_TaskDelay(0);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestTaskDelete(uintptr_t tasKId)
{
    U32 ret = PRT_TaskDelete((TskHandle)tasKId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE void *TestMalloc(unsigned int size)
{
    return PRT_MemAlloc(OS_MID_SYS, 0, size);
}

STATIC_INLINE void TestFree(void *mem)
{
    (void)PRT_MemFree(OS_MID_SYS, mem);
}

STATIC_INLINE int TestMessageQueueCreate(unsigned int nodeNum, unsigned int maxNodeSize, uintptr_t *queueId)
{
    U32 ret;

    ret = PRT_QueueCreate(nodeNum, maxNodeSize, (U32 *)queueId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestMessageQueueSend(uintptr_t queueId, char *buff, unsigned int size)
{
    U32 ret;

    ret = PRT_QueueWrite((U32)queueId, buff, size, OS_QUEUE_WAIT_FOREVER, OS_QUEUE_NORMAL);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestMessageQueueReceive(uintptr_t queueId, char *buff, unsigned int *size)
{
    U32 ret;

    ret = PRT_QueueRead((U32)queueId, buff, size, OS_QUEUE_WAIT_FOREVER);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestMessageQueueDelete(uintptr_t queueId)
{
    U32 ret;

    ret = PRT_QueueDelete((U32)queueId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestSemaphoreCreate(unsigned int count, uintptr_t *semId)
{
    U32 ret;

    ret = PRT_SemCreate(count, (SemHandle *)semId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestSemaphoreObtain(uintptr_t semId, uint32_t waitTime)
{
    U32 ret;

    ret = PRT_SemPend((SemHandle)semId, OS_WAIT_FOREVER);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestSemaphoreRelease(uintptr_t semId)
{
    U32 ret;

    ret = PRT_SemPost((SemHandle)semId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestSemaphoreDelete(uintptr_t semId)
{
    U32 ret;

    ret = PRT_SemDelete((SemHandle)semId);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

#define TEST_HWI_IRQ_NUM  51
#define TEST_HWI_IRQ_PROT 0
STATIC_INLINE int TestHwiCreate(uint32_t irqNum, TEST_HWI_FUNC func)
{
    U32 ret;

    ret = PRT_HwiSetAttr(irqNum, TEST_HWI_IRQ_PROT, OS_HWI_MODE_ENGROSS);
    if (ret != OS_OK) {
        return -1;
    }

    ret = PRT_HwiCreate(irqNum, (HwiProcFunc)func, (U32)TEST_HWI_IRQ_NUM);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestHwiEnable(uint32_t irqNum)
{
    U32 ret;

    ret = PRT_HwiEnable(irqNum);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestHwiDisable(uint32_t irqNum)
{
    U32 ret;

    ret = PRT_HwiDisable(irqNum);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestHwiTrigger(uint32_t irqNum)
{
    NVIC_SetPendingIRQ((IRQn_Type)irqNum);
    return 0;
}

STATIC_INLINE int TestHwiClearPending(uint32_t irqNum)
{
    U32 ret;

    ret = PRT_HwiClearPendingBit(irqNum);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}

STATIC_INLINE int TestHwiDelete(uint32_t irqNum)
{
    U32 ret;

    ret = PRT_HwiDelete((HwiHandle)irqNum);
    if (ret != OS_OK) {
        return -1;
    }
    return 0;
}
#endif
