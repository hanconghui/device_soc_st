/*
 * Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HAL_RTOS_H
#define HAL_RTOS_H
/*
 * 内存分配适配层
 *
 * 分配内存函数
 * void *TestMalloc(unsigned int size);
 *
 * 释放内存函数
 * void TestFree(void *mem);
 */


/*
 * 任务性能测试适配层
 *
 * 创建任务并允许函数
 * int TestTaskCreate(const char *name, int priority, unsigned int stackSize, TEST_TASK_FUNC func, uintptr_t *taskId);
 *
 * 挂起任务函数
 * int TestTaskSuspend(uintptr_t taskId);
 *
 * 唤醒任务函数
 * int TestTaskResume(uintptr_t taskId);
 *
 * 主动让出cpu
 * int TestTaskYield(void);
 *
 * 删除任务函数
 * int TestTaskDelete(uintptr_t taskId);
 */

/*
 * 中断性能测试适配层
 *
 * 创建中断函数
 * int TestHwiCreate(int irqNum, int priority, TEST_HWI_FUNC func);
 *
 * 使能中断函数
 * int TestHwiEnable(int irqNum);
 *
 * 去使能中断函数
 * int TestHwiDisable(int irqNum);
 *
 * 触发中断响应函数
 * int TestHwiTrigger(int irqNum);
 *
 * 清除中断阻塞状态函数
 * int TestHwiClearPending(int irqNum);
 */

#ifdef __UNIPROTON__
#include "hal_uniproton.h"
#else
#error "No support rtos kernel !"
#endif

#endif