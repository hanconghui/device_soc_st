/*
* Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of
*    conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
*    of conditions and the following disclaimer in the documentation and/or other materials
*    provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
*    to endorse or promote products derived from this software without specific prior written
*    permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* *EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "rhealstone_test.h"
#include "hal/hal_rtos.h"

#define TEST_LOOP 10
#define STACK_SIZE 512
#define TEST_COUNT 50000

void RhealstonePutTime(uint32_t count, uint32_t totalTime, uint32_t loopTime,
                       uint32_t overheadTime, uint32_t result[TEST_RESULT_NUM])
{
    if (count != 0) {
        uint32_t average = ((totalTime - loopTime) / count);
        if (average < overheadTime) {
            PRINT_INFO("totalTime: %u loopTime: %u average: %u overheadTime: %u\n", \
                       totalTime, loopTime, average, overheadTime);
            result[0] = (uint32_t)-1;
            result[1] = (uint32_t)-1;
        } else {
            result[TEST_RESULT] = average;
            result[TEST_RESULT_HEAD] = (average - overheadTime);
        }
    }
}

void RunTestCase(const char *caseName, TaskPreempt *taskInfo, int (*TestCase)(TaskPreempt *, uint32_t *))
{
    int ret;
    uint32_t testResult[TEST_RESULT_NUM];
    uint64_t sum[TEST_RESULT_NUM] = {0};
    uint32_t count[TEST_RESULT_NUM] = {0};

    for (count[TEST_COUNT_BASE] = 0; count[TEST_COUNT_BASE] < TEST_LOOP; count[TEST_COUNT_BASE]++) {
        printf("\r%-40s TestCount=%u ", caseName, count[TEST_COUNT_BASE]);
        int result = memset_s(testResult, sizeof(testResult), 0, sizeof(testResult));
        if (result != 0) {
            printf("memset_s failed!,err = %d\n", result);
            return;
        }

        ret = TestCase(taskInfo, testResult);
        if (ret < 0) {
            printf("Run %s failed!\n", caseName);
            return;
        }

        if (testResult[TEST_RESULT] != (uint32_t)-1) {
            sum[TEST_RESULT] += testResult[TEST_RESULT];
        }

        if (testResult[TEST_RESULT_HEAD] != (uint32_t)-1) {
            sum[TEST_RESULT_HEAD] += testResult[TEST_RESULT_HEAD];
        } else {
            count[TEST_COUNT_FAILED]++;
        }
    }
    RhealstoneTestTimeShow(caseName, count, sum);
}

static void TestInit(void)
{
    TaskPreempt taskInfo = {0};
#ifdef __UNIPROTON__
    printf("############### Uniproton test ###############\n");
#elif __FREERTOS__
    printf("############### FreeRTOS test ###############\n");
#endif

#ifdef __UNIPROTON__
    #define HIGH_PRIO 2
    #define MID_PRIO 3
    #define LOW_PRIO 4

    taskInfo.highPrio = HIGH_PRIO;
    taskInfo.midPrio = MID_PRIO;
    taskInfo.lowPrio = LOW_PRIO;
    taskInfo.stackSize = 0x800;
#elif __FREERTOS__
    #define HIGH_PRIO 4
    #define MID_PRIO 3
    #define LOW_PRIO 2

    taskInfo.highPrio = HIGH_PRIO;
    taskInfo.midPrio = MID_PRIO;
    taskInfo.lowPrio = LOW_PRIO;
    taskInfo.stackSize = STACK_SIZE;
#endif

    taskInfo.testCount = TEST_COUNT;

    RunTestCase("Rhealstone: Task switch", &taskInfo, RhealstoneSwitchTest);

    RunTestCase("Rhealstone: Task Preempt", &taskInfo, RhealstonePreemptTest);

    RunTestCase("Rhealstone: Intertask Message Latency", &taskInfo, RhealstoneMessageLatencyTest);

    RunTestCase("Rhealstone: Semaphore Shuffle", &taskInfo, RhealstoneSemaphoreShuffleTest);

    RunTestCase("Rhealstone: Deadlock Break", &taskInfo, RhealstoneDeadlockBreakTest);

    RunTestCase("Rhealstone: Interrupt Latency", &taskInfo, RhealstoneInterruptLatencyTest);
}

void RhealstoneTest(void)
{
    uint32_t testHandle = 0;
    int ret;
    int priority;
    int stackSize;
#ifdef __UNIPROTON__
    #define PRIORITY 25

    priority = PRIORITY;
    stackSize = 0x800;
#elif __FREERTOS__
    #define PRIORITY 1

    priority = PRIORITY;
    stackSize = STACK_SIZE;
#endif
    ret = TestTaskCreate("TestInit", priority, stackSize, (TEST_TASK_FUNC)TestInit, &testHandle);
    if (ret < 0) {
        printf("TestTaskCreate TestInit failed!\n");
        return;
    }

    ret = TestTaskResume(testHandle);
    if (ret < 0) {
        printf("TestTaskResume TestInit failed!\n");
        return;
    }
    return;
}