/*
* Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of
*    conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
*    of conditions and the following disclaimer in the documentation and/or other materials
*    provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
*    to endorse or promote products derived from this software without specific prior written
*    permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* *EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "hal_rtos.h"

static volatile uint32_t g_testCountMax = 10;
static volatile uint32_t g_testCount;
static uint32_t g_totalTime;
static uintptr_t g_taskId;

static void ClearEnvironment(void)
{
    g_taskId = 0;
    g_totalTime = 0;
}

static void BaseTimeDelay(void)
{
    int i;
    int delayLoop = 100;
    for (i = 0; i < delayLoop; i++);
}

static void BaseTimeTaskFunc(uintptr_t arg)
{
    int ret;
    PRINT_INFO("%s start !!!\n", __FUNCTION__);

    ret = TestTimeInit();
    TEST_RESULT_CHECK_GOTO(ret, "TestTimeInit failed", EXIT);

    for (g_testCount = 0; g_testCount < g_testCountMax; g_testCount++) {
        BaseTimeDelay();
    }

    g_totalTime = TestTimeGet();

EXIT:
    TestTaskDelete(g_taskId);
}

int RhealstoneBaseTime(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM])
{
    int ret;
    if (taskInfo == NULL) {
        return -1;
    }

    if (taskInfo->testCount != 0) {
        g_testCountMax = taskInfo->testCount;
    }
    ClearEnvironment();

    ret = TestTaskCreate("BaseTime", taskInfo->highPrio, taskInfo->stackSize, BaseTimeTaskFunc, &g_taskId);
    TEST_RESULT_CHECK_GOTO(ret, "Test create BaseTime task failed!", EXIT);

    ret = TestTaskResume(g_taskId);
    TEST_RESULT_CHECK_GOTO(ret, "TestTaskResume BaseTime task failed", EXIT);

    RhealstonePutTime(g_testCountMax, g_totalTime, 0, 0, testResult);

    TestTaskDelete(g_taskId);
    return 0;

EXIT:
    TestTaskDelete(g_taskId);
    return -1;
}