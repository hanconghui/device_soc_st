/*
* Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of
*    conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
*    of conditions and the following disclaimer in the documentation and/or other materials
*    provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
*    to endorse or promote products derived from this software without specific prior written
*    permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* *EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "hal_rtos.h"

static volatile uint32_t g_testCountMax = 10;
static volatile uint32_t g_testCount;
static uintptr_t g_taskId[2];
static uint32_t g_totalTime;
static uint32_t g_loopTime;
static uint32_t g_testTaskExit;
static uintptr_t g_semId;
static uint32_t g_semExe;
#define EXIT_FLAG 2
#define TASK_NUM 2
static void ClearEnvironment(void)
{
    int ret = memset_s(g_taskId, sizeof(g_taskId), 0, sizeof(uintptr_t) * TASK_NUM);
    if (ret != 0) {
        printf("memset_s failed!,err = %d\n", ret);
        return;
    }
    g_loopTime = 0;
    g_totalTime = 0;
    g_testTaskExit = 0;
    g_semId = 0;
    g_semExe = 0;
}

static void TaskFunc01(uintptr_t arg)
{
    int ret;

    if (g_semExe == 0) {
        ret = TestTaskResume(g_taskId[1]);
        TEST_RESULT_CHECK_GOTO(ret, "TestTaskResume failed", EXIT);
    } else {
        ret = TestTaskResume(g_taskId[1]);
        TEST_RESULT_CHECK_GOTO(ret, "TestTaskResume failed", EXIT);
    }
#ifndef __FREERTOS__
    TestTaskYield();
#endif

    for (; g_testCount < g_testCountMax;) {
        if (g_semExe == 1) {
            ret = TestSemaphoreObtain(g_semId, 0xFFFFFFFF);
            TEST_RESULT_CHECK_GOTO(ret, "TestSemaphoreObtain failed", EXIT);
        }
        TestTaskYield();

        if (g_semExe == 1) {
            ret = TestSemaphoreRelease(g_semId);
            TEST_RESULT_CHECK_GOTO(ret, "TestSemaphoreRelease failed", EXIT);
        }
        TestTaskYield();
    }

EXIT:
    g_testTaskExit++;
    TestTaskDelete(g_taskId[0]);
}

static void TaskFunc02(uintptr_t arg)
{
    int ret;

    ret = TestTimeInit();
    TEST_RESULT_CHECK_GOTO(ret, "TestTimeInit failed", EXIT);

    for (g_testCount = 0; g_testCount < g_testCountMax; g_testCount++) {
        if (g_semExe == 1) {
            ret = TestSemaphoreObtain(g_semId, 0xFFFFFFFF);
            TEST_RESULT_CHECK_GOTO(ret, "TestSemaphoreObtain failed", EXIT);
        }
        TestTaskYield();

        if (g_semExe == 1) {
            ret = TestSemaphoreRelease(g_semId);
            TEST_RESULT_CHECK_GOTO(ret, "TestSemaphoreRelease failed", EXIT);
        }
        TestTaskYield();
    }
    g_totalTime = TestTimeGet();

    if (g_semExe == 0) {
        g_loopTime = g_totalTime;
        ret = TestTaskSuspend(g_taskId[0]);
        TEST_RESULT_CHECK_GOTO(ret, "TestTaskSuspend failed", EXIT);
        ret = TestTaskSuspend(g_taskId[1]);
        TEST_RESULT_CHECK_GOTO(ret, "TestTaskSuspend failed", EXIT);
    }

EXIT:
    g_testTaskExit++;
    TestTaskDelete(g_taskId[1]);
}

int RhealstoneSemaphoreShuffleTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM])
{
    int ret;
    if (taskInfo == NULL) {
        return -1;
    }

    if (taskInfo->testCount != 0) {
        g_testCountMax = taskInfo->testCount;
    }

    ClearEnvironment();

    ret = TestSemaphoreCreate(1, &g_semId);
    TEST_RESULT_CHECK_GOTO(ret, "TestSemaphoreCreate failed!", EXIT);

    ret = TestTaskCreate("Task01", taskInfo->lowPrio, taskInfo->stackSize, TaskFunc01, &g_taskId[0]);
    TEST_RESULT_CHECK_GOTO(ret, "Test create low task failed!", EXIT);

    ret = TestTaskCreate("Task02", taskInfo->lowPrio, taskInfo->stackSize, TaskFunc02, &g_taskId[1]);
    TEST_RESULT_CHECK_GOTO(ret, "Test create hi task failed!", EXIT);

    g_semExe = 0;
    ret = TestTaskResume(g_taskId[0]);
    TEST_RESULT_CHECK_GOTO(ret, "Test resume low task failed!", EXIT);

    ret = TestTaskDelete(g_taskId[0]);
    TEST_RESULT_CHECK_GOTO(ret, "TestTaskDelete failed!", EXIT);
    ret = TestTaskDelete(g_taskId[1]);
    TEST_RESULT_CHECK_GOTO(ret, "TestTaskDelete failed!", EXIT);

    ret = TestTaskCreate("Task01", taskInfo->lowPrio, taskInfo->stackSize, TaskFunc01, &g_taskId[0]);
    TEST_RESULT_CHECK_GOTO(ret, "Test create low task failed!", EXIT);

    ret = TestTaskCreate("Task02", taskInfo->lowPrio, taskInfo->stackSize, TaskFunc02, &g_taskId[1]);
    TEST_RESULT_CHECK_GOTO(ret, "Test create hi task failed!", EXIT);

    g_semExe = 1;
    ret = TestTaskResume(g_taskId[0]);
    TEST_RESULT_CHECK_GOTO(ret, "Test resume low task failed!", EXIT);

    TEST_RESULT_ASSERT(g_testTaskExit, EXIT_FLAG, EXIT);

    ret= TestSemaphoreDelete(g_semId);
    TEST_RESULT_CHECK_GOTO(ret, "Test TestMessageQueueDelete failed!", EXIT);

    RhealstonePutTime(g_testCountMax * TASK_NUM, g_totalTime, g_loopTime, 0, testResult);
    TestTaskDelete(g_taskId[0]);
    TestTaskDelete(g_taskId[1]);
    return 0;

EXIT:
    TestTaskDelete(g_taskId[0]);
    TestTaskDelete(g_taskId[1]);
    TestSemaphoreDelete(g_semId);
    return -1;
}