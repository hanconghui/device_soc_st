/*
* Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of
*    conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
*    of conditions and the following disclaimer in the documentation and/or other materials
*    provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
*    to endorse or promote products derived from this software without specific prior written
*    permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* *EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "hal_rtos.h"

static volatile uintptr_t g_testCountMax = 10;
static volatile uintptr_t g_testCount;
static uintptr_t g_taskId;
static uint32_t g_overheadTime;
static uint32_t g_totalTime;
static uint32_t g_responseTime;

static void ClearEnvironment(void)
{
    g_taskId = 0;
    g_overheadTime = 0;
    g_totalTime = 0;
    g_responseTime = 0;
}

void RhealstoneHwiFunc(void)
{
    int ret;
    g_responseTime = TestTimeGet();
    ret = TestHwiClearPending(TEST_HWI_IRQ_NUM);
    if (ret < 0) {
        printf("TestHwiClearPending clear failed!\n\r");
    }
}

static void HwiTaskFunc(uintptr_t arg)
{
    int ret;
    ret = TestHwiCreate(TEST_HWI_IRQ_NUM, RhealstoneHwiFunc);
    TEST_RESULT_CHECK_GOTO(ret, "HwiCreate failed", EXIT);

    g_totalTime = 0;
    for (g_testCount = 0; g_testCount < g_testCountMax; g_testCount++) {
        ret = TestHwiEnable(TEST_HWI_IRQ_NUM);
        TEST_RESULT_CHECK_GOTO(ret, "HwiEnable failed", EXIT);

        ret = TestTimeInit();
        TEST_RESULT_CHECK_GOTO(ret, "TestTimeInit failed", EXIT);

        ret = TestHwiTrigger(TEST_HWI_IRQ_NUM);
        TEST_RESULT_CHECK_GOTO(ret, "TestHwiTrigger failed", EXIT);

        ret = TestHwiDisable(TEST_HWI_IRQ_NUM);
        TEST_RESULT_CHECK_GOTO(ret, "HwiDisable failed", EXIT);

        g_totalTime += g_responseTime;
    }

EXIT:
    TestTaskDelete(g_taskId);
}

int RhealstoneInterruptLatencyTest(TaskPreempt *taskInfo, uint32_t testResult[TEST_RESULT_NUM])
{
    int ret;
    if (taskInfo->testCount != 0) {
        g_testCountMax = taskInfo->testCount;
    }

    ClearEnvironment();

    ret = TestTaskCreate("HwiTask", taskInfo->highPrio, taskInfo->stackSize, HwiTaskFunc, &g_taskId);
    if (ret < 0) {
        printf("[%s:%d] Test create hi task failed!\n", __FUNCTION__, __LINE__);
        return -1;
    }

    ret = TestTaskResume(g_taskId);
    TEST_RESULT_CHECK_GOTO(ret, "TestTaskResume Task01 task failed", EXIT);

    ret = TestHwiDelete(TEST_HWI_IRQ_NUM);
    TEST_RESULT_CHECK_GOTO(ret, "TestHwiDelete failed", EXIT);

    ret = TestTimeInit();
    TEST_RESULT_CHECK_GOTO(ret, "TestTimeInit failed", EXIT);
    TestTimeGet();
    ret = TestTimeInit();
    TEST_RESULT_CHECK_GOTO(ret, "TestTimeInit failed", EXIT);
    g_overheadTime = TestTimeGet();

    RhealstonePutTime(g_testCountMax, g_totalTime, 0, g_overheadTime, testResult);

    TestTaskDelete(g_taskId);
    return 0;

EXIT:
    TestTaskDelete(g_taskId);
    TestHwiDelete(TEST_HWI_IRQ_NUM);
    return -1;
}
