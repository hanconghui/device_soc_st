/*
* Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of
*    conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
*    of conditions and the following disclaimer in the documentation and/or other materials
*    provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
*    to endorse or promote products derived from this software without specific prior written
*    permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* *EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "hal_rtos.h"
#include "hal_test.h"

#include <stdint.h>
#include <prt_tick.h>
#include <prt_task.h>
#include <prt_sys.h>

#define directive_failed(_dirstat, failmsg) do { \
    if ((_dirstat) != OS_OK) {  \
        printf("\n%s FAILED --- expected (%d) got (%d)\n", (failmsg), OS_OK, _dirstat); \
        return;  \
    }            \
} while (0)

#define rtems_test_assert(__exp) do {                    \
    if (!(__exp)) {                                      \
        printf("%s: %d %s\n", __FILE__, __LINE__, #__exp); \
        PRT_SysReboot();                                 \
    }                                                    \
} while (0)

#define put_time(_message, _total_time, _iterations, _loop_overhead, _overhead) \
    printf("%s : %d \n", _message, (((_total_time) - (_loop_overhead)) / (_iterations)) - (_overhead))

void benchmark_timer_initialize(void)
{
    TestTimeInit();
}

uint32_t benchmark_timer_read(void)
{
    return TestTimeGet();
}

#define BENCHMARKS 50000

void Task02(uintptr_t param1, uintptr_t param2, uintptr_t param3, uintptr_t param4);
void Task01(uintptr_t param1, uintptr_t param2, uintptr_t param3, uintptr_t param4);

TskHandle Task_id[2];
const char *Task_name[2];

uint32_t telapsed;
uint32_t tloop_overhead;
uint32_t tswitch_overhead;

unsigned long count1;
U32 status;

void Task01(uintptr_t param1, uintptr_t param2, uintptr_t param3, uintptr_t param4)
{
    status = PRT_TaskResume(Task_id[1]);
    directive_failed(status, "PRT_TaskResume of TA02");

    tswitch_overhead = benchmark_timer_read();

    benchmark_timer_initialize();
    for (count1 = 0; count1 < BENCHMARKS; count1++) {
        PRT_TaskResume(Task_id[1]);
    }

    PRT_TaskDelete(Task_id[0]);
}

void Task02(uintptr_t param1, uintptr_t param2, uintptr_t param3, uintptr_t param4)
{
    benchmark_timer_initialize();
    PRT_TaskSuspend(Task_id[1]);

    for (; count1 < BENCHMARKS - 1;) {
        PRT_TaskSuspend(Task_id[1]);
    }

    telapsed = benchmark_timer_read();

    put_time(
      "Rhealstone: Task Preempt",
      telapsed,
      BENCHMARKS - 1,
      tloop_overhead,
      tswitch_overhead
    );

    PRT_TaskDelete(Task_id[1]);
}

void RhealstoneInit(void)
{
    struct TskInitParam taskParam = {0};

    taskParam.taskEntry = (TskEntryFunc)Task01;
    taskParam.stackSize = 0x800;
    taskParam.name = "TA01";
    taskParam.taskPrio = OS_TSK_PRIORITY_08;
    taskParam.stackAddr = 0;

    status = PRT_TaskCreate(&Task_id[0], &taskParam);
    directive_failed(status, "PRT_TaskCreate of TA01");

    taskParam.taskEntry = (TskEntryFunc)Task02;
    taskParam.stackSize = 0x800;
    taskParam.name = "TA02";
    taskParam.taskPrio = OS_TSK_PRIORITY_05;
    taskParam.stackAddr = 0;

    status = PRT_TaskCreate(&Task_id[1], &taskParam);
    directive_failed(status, "PRT_TaskCreate of TA02");

    benchmark_timer_initialize();
    for (count1 = 0; count1 < BENCHMARKS; count1++) {
        asm volatile("");
    }

    tloop_overhead = benchmark_timer_read();
    status = PRT_TaskResume(Task_id[0]);
    directive_failed(status, "PRT_TaskResume of TA01");

    PRT_TaskDelete(Task_id[0]);
    PRT_TaskDelete(Task_id[1]);
}
