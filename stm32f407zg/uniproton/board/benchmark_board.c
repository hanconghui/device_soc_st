/*
* Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of
*    conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
*    of conditions and the following disclaimer in the documentation and/or other materials
*    provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
*    to endorse or promote products derived from this software without specific prior written
*    permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* *EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "prt_task.h"
#include "prt_config.h"
#include "securec.h"
#include "pthread.h"
#include "prt_sem.h"

#define BAUDRATE        115200
#define DELAY_TIME      1000
#define DEFAULT_MOV     8
#define RECURSIVE_MOV   4

extern unsigned long g_data_start;
extern unsigned long g_data_end;
extern unsigned long data_load_start;

#ifdef __cpluscplus
#if __cpluscplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

U32 PRT_HardDrvInit(void)
{
    UartInit(BAUDRATE);
    return OS_OK;
}

void PRT_HardBootInit(void)
{
    int size = (unsigned long)&g_data_end - (unsigned long)&g_data_start;
    int ret = memcpy_s((void*)&g_data_start, size, (void*)&data_load_start, size);
    if (ret != 0) {
        printf("memcpy_s failed.\r\n");
    }
}

static void OsTskUser1(void)
{
    printf("OsTskUser1:\n\r");

    while (TRUE) {
        printf("OsTskUser1 loop\n\r");
        PRT_TaskDelay(DELAY_TIME);
    }
}

static void OsTskUser2(void)
{
    printf("OsTskUser2:\n\r");

    while (TRUE) {
        printf("OsTskUser2 loop\n\r");
        PRT_TaskDelay(DELAY_TIME / 2); /* 2, delay */
    }
}

static void TestTask(void)
{
    printf("TestTask!!!\n");

    U32 ret;
    TskHandle taskId1;
    TskHandle taskId2;
    struct TskInitParam taskParam1 = {0};
    struct TskInitParam taskParam2 = {0};

    taskParam1.taskEntry = (TskEntryFunc)OsTskUser1;
    taskParam1.stackSize = 0x800;
    taskParam1.name = "UserTask1";
    taskParam1.taskPrio = OS_TSK_PRIORITY_05;
    taskParam1.stackAddr = 0;

    taskParam2.taskEntry = (TskEntryFunc)OsTskUser2;
    taskParam2.stackSize = 0x800;
    taskParam2.name = "UserTask2";
    taskParam2.taskPrio = OS_TSK_PRIORITY_05;
    taskParam2.stackAddr = 0;

    ret = PRT_TaskCreate(&taskId1, &taskParam1);
    if (ret != OS_OK) {
        return ret;
    }

    ret = PRT_TaskResume(&taskId1);
    if (ret != OS_OK) {
        return ret;
    }

    ret = PRT_TaskCreate(&taskId2, &taskParam2);
    if (ret != OS_OK) {
        return ret;
    }
    ret = PRT_TaskResume(&taskId2);
    if (ret != OS_OK) {
        return ret;
    }
}

U32 PRT_AppInit(void)
{
    printf("PRT_AppInit!\n");
    RTOSPerformanceTest();
    return OS_OK;
}

int pthread_mutex_lock(pthread_mutex_t *__mutex)
{
    return 0; 
}

int pthread_mutex_unlock(pthread_mutex_t *__mutex)
{
    return 0;
}

#ifdef __cpluscplus
#if __cpluscplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */